package cabezas.manel.t3ej5;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {
    TextView txt;
    ImageButton btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt = (TextView) findViewById(R.id.textview1);
        btn = (ImageButton) findViewById(R.id.button1);
        btn.setOnCreateContextMenuListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void onGroupItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.group_item1:
                txt.setTextColor(getResources().getColor(R.color.red));
                btn.setColorFilter(getResources().getColor(R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
                break;
            case R.id.group_item2:
                txt.setTextColor(getResources().getColor(R.color.green));
                btn.setColorFilter(getResources().getColor(R.color.green), android.graphics.PorterDuff.Mode.MULTIPLY);
                break;
            case R.id.group_item3:
                txt.setTextColor(getResources().getColor(R.color.blue));
                btn.setColorFilter(getResources().getColor(R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                break;
            default:
                txt.setTextColor(getResources().getColor(android.R.color.background_dark));
        }

    }

    //--MENU CONTEXTUAL--

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Toast.makeText(this, item.getTitle() + "Menu Contextual", Toast.LENGTH_SHORT).show();
        return true;
    }
}
